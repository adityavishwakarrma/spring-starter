#!/bin/bash

mkdir -p /data/db
mkdir log
mongod --fork --logpath log/mongod.log --bind_ip 0.0.0.0
ps -aux | grep mongod

cd /code/sample-data
./load-sample-data.sh
# cd -
# java -jar /code/build/libs/spring-starter-0.0.1-SNAPSHOT.jar

#  sudo apt-get update && sudo apt-get install -y mysql-server && sudo service mysql restart && sudo mysql
# cd -
cd ..
# java -jar /code/build/libs/*.jar
 ./gradlew bootrun
 
