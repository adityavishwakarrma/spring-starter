FROM gradle:jdk11

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install git redis-server wget

RUN apt-get install -y gnupg
RUN apt-get update
RUN apt-get install -y mongodb

USER root

RUN mkdir code
COPY . /code

RUN cd /code && ./gradlew bootjar

CMD /code/start.sh
